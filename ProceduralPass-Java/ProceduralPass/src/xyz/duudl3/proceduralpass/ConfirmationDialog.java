package xyz.duudl3.proceduralpass;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class ConfirmationDialog extends Canvas
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4994322898092431234L;
	public static JFrame frame;

	public ConfirmationDialog(String actionType)
	{
		JPanel mainPanel = new JPanel();
		this.setPreferredSize(new Dimension(350, 200));
		this.setMaximumSize(new Dimension(350, 200));
		this.setMinimumSize(new Dimension(350, 200));

		/***LABEL FIELD START***/
		JLabel label = new JLabel("Label Text");
		label.setHorizontalAlignment(SwingConstants.CENTER);

		if(actionType == "Delete")
		{
			label.setText("Are you sure you wish to delete?");
		}
		else if(actionType == "Reset")
		{
			label.setText("Are you sure you wish to reset?");
		}
		
		if(MainWindow.INSTANCE.getOS().toLowerCase().contains("linux"))
		{
			label.setFont(new Font("Arial", Font.PLAIN, 18));
			label.setBounds(0, 45, 350, 25);
		}
		else
		{
			label.setFont(new Font("Arial", Font.PLAIN, 18));
			label.setBounds(0, 45, 350, 25);
		}

		label.setForeground(Color.white);
		label.setBorder(null);
		label.setOpaque(false);
		label.setBackground(new Color(0, 0, 0, 0));
		/***LABEL FIELD END***/

		/***CONFIRM BUTTON START**/
		JButton confirm = new JButton("Yes");
		confirm.setFont(new Font("Arial", Font.PLAIN, 14));
		confirm.setBorder(null);
		confirm.setFocusable(false);
		confirm.setFocusPainted(false);
		confirm.setBorderPainted(false);
		confirm.setForeground(Color.white);
		confirm.setBackground(Color.decode("#666666"));
		confirm.setContentAreaFilled(true);
		confirm.setBounds((int)(this.getMaximumSize().getWidth() / 2) - (120/2) - 65, (int)this.getMaximumSize().getHeight() - 75, 120, 30);
		confirm.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				if(App.masterPassword.length() != 0)
				{
					if(actionType == "Delete")
					{
						Logins.INSTANCE.deleteLogin();
					}
					else if(actionType == "Reset")
					{
						Logins.INSTANCE.resetPassword();
					}
					
					close();
				}
			}
		});
		/***CONFIRM BUTTON END***/
		
		/***DECLINE BUTTON START**/
		JButton decline = new JButton("No");
		decline.setFont(new Font("Arial", Font.PLAIN, 14));
		decline.setBorder(null);
		decline.setFocusable(false);
		decline.setFocusPainted(false);
		decline.setBorderPainted(false);
		decline.setForeground(Color.white);
		decline.setBackground(Color.decode("#666666"));
		decline.setContentAreaFilled(true);
		decline.setBounds((int)(this.getMaximumSize().getWidth() / 2) - (120/2) + 65, (int)this.getMaximumSize().getHeight() - 75, 120, 30);
		decline.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				if(App.masterPassword.length() != 0)
				{
					Logins.INSTANCE.table.clearSelection();
					close();
				}
			}
		});
		/***CONFIRM BUTTON END***/

		mainPanel.setBackground(Color.decode("#1D1D1D"));

		mainPanel.setOpaque(true);
		this.setBackground(Color.decode("#1D1D1D"));
		mainPanel.add(this);
		mainPanel.add(label);
		mainPanel.add(confirm);
		mainPanel.add(decline);
		mainPanel.setLayout (null); 

		frame = new JFrame("Alert");
		frame.setSize(350, 200);
		frame.getContentPane().add(mainPanel);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		frame.setUndecorated(false);
	}

	public void show()
	{
		frame.setVisible(true);
	}

	public void hide()
	{
		frame.setVisible(false);
	}
	
	public void close()
	{
		frame.dispose();
		frame = null;
	}
}
