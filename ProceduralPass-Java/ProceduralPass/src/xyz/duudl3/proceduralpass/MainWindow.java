package xyz.duudl3.proceduralpass;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;

public class MainWindow extends Canvas implements Runnable, KeyListener
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2062095875076661163L;
	private boolean running = false;
	private Thread thread;
	public static JFrame frame;
	public static String version = "v1.4.3";
	public static String title = "Procedural Pass";
	private JPasswordField pass;
	private String os = "";
	public static MainWindow INSTANCE;
	public boolean keepUpdatingPassString = true;

	public static void main(String[] args)
	{
		MainWindow main = new MainWindow();
		main.show();
	}

	public String getOS()
	{
		return os;
	}

	public MainWindow()
	{
		INSTANCE = this;
		os = System.getProperty("os.name");

		JPanel mainPanel = new JPanel();
		this.setPreferredSize(new Dimension(350, 200));
		this.setMaximumSize(new Dimension(350, 200));
		this.setMinimumSize(new Dimension(350, 200));

		/***LABEL FIELD START***/
		JLabel label = new JLabel("Master Password");
		label.setHorizontalTextPosition(JButton.CENTER);
		label.setVerticalTextPosition(JButton.CENTER);
		if(getOS().toLowerCase().contains("linux"))
		{
			label.setFont(new Font("Arial", Font.PLAIN, 18));
			label.setBounds((int)(this.getMaximumSize().getWidth() / 2) - (155/2), 45, 155, 25);
		}
		else
		{
			label.setFont(new Font("Arial", Font.PLAIN, 24));
			label.setBounds((int)(this.getMaximumSize().getWidth() / 2) - (185/2), 45, 185, 25);
		}

		label.setForeground(Color.white);
		label.setBorder(null);
		label.setOpaque(false);
		label.setBackground(new Color(0, 0, 0, 0));
		/***LABEL FIELD END***/

		/***PASSWORD FIELD START***/
		pass = new JPasswordField("");
		pass.setBorder(null);
		pass.setBounds((int)(this.getMaximumSize().getWidth() / 2) - 100, (int)(this.getMaximumSize().getHeight() / 2) - 15, 200, 30);
		pass.setBackground(Color.decode("#3C3C3C"));
		pass.setForeground(Color.white);
		pass.setCaretColor(Color.white);
		pass.addKeyListener(this);

		/***PASSWORD FIELD END***/

		/***LOGIN BUTTON START**/
		JButton open = new JButton("Open");
		open.setFont(new Font("Arial", Font.PLAIN, 14));
		open.setBorder(null);
		open.setBorderPainted(false);
		open.setForeground(Color.white);
		open.setBackground(Color.decode("#666666"));
		open.setContentAreaFilled(true);
		open.setBounds((int)(this.getMaximumSize().getWidth() / 2) - (120/2), (int)this.getMaximumSize().getHeight() - 75, 120, 30);
		open.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				if(App.masterPassword.length() != 0)
				{
					keepUpdatingPassString = false;
					Logins logins = new Logins();
					logins.show();
					hide();
				}
			}
		});
		/***LOGIN BUTTON END***/

		mainPanel.setBackground(Color.decode("#1D1D1D"));

		mainPanel.setOpaque(true);
		this.setBackground(Color.decode("#1D1D1D"));
		mainPanel.add(this);
		mainPanel.add(label);
		mainPanel.add(open);
		mainPanel.add(pass);
		mainPanel.setLayout (null); 

		frame = new JFrame(title + " " + version);
		frame.setSize(350, 200);
		frame.getContentPane().add(mainPanel);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		frame.setUndecorated(false);
		this.start();

		LoginArrayHandler loginHandler = new LoginArrayHandler();

		loginHandler.loadLogins();
		loginHandler.saveLogins();
	}

	public void show()
	{
		frame.setVisible(true);
	}

	public void hide()
	{
		pass.setText("000000000");
        App.masterPassword = LoginEntry.SHA256(App.masterPassword + "&GH&*BT6VR6r6v6&B&H7H7h&H77ybn&y7TBV");
		frame.setVisible(false);
	}

	public synchronized void start()
	{
		if(this.running)
			return;

		this.running = true;
		this.thread = new Thread(this);
		this.thread.start();	
	}

	@SuppressWarnings("unused")
	@Override
	public void run() {
		long lastTime = System.nanoTime();
		double amountOfTicks = 60.0;
		double ns = 1000000000 / amountOfTicks;
		double delta = 0;
		long timer = System.currentTimeMillis();
		int updates = 0;
		int frames = 0;

		while(running){
			long now = System.nanoTime();
			delta += (now - lastTime) / ns;
			lastTime = now;
			while(delta >= 1){
				tick();
				updates++;
				delta--;
			}
			render();
			frames++;

			if(System.currentTimeMillis() - timer > 1000){
				timer += 1000;
				//System.out.println("FPS: " + frames + " TICKS: " + updates);
				frames = 0;
				updates = 0;
			}
		}
	}

	private void tick()
	{
		if(keepUpdatingPassString)
		{
			App.masterPassword = new String(pass.getPassword());
		}
	}

	private void render()
	{

	}

	@Override
	public void keyPressed(KeyEvent ke) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyReleased(KeyEvent ke) {		
		//10 is the enter key
		if(ke.getKeyCode() == 10)
		{
			if(App.masterPassword.length() != 0)
			{
				keepUpdatingPassString = false;
				Logins logins = new Logins();
				logins.show();
				hide();
			}
		}
	}

	@Override
	public void keyTyped(KeyEvent ke) {
		// TODO Auto-generated method stub
		
	}
}
