﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProceduralPass
{
    /// <summary>
    /// Interaction logic for Logins.xaml
    /// </summary>
    public partial class Logins : Window
    {
        public static Logins INSTANCE;

        public Logins()
        {
            InitializeComponent();
            INSTANCE = this;

            LoginDataGrid.ItemsSource = LoginArrayHandler.INSTANCE.logins;
        }

        private void openNewLoginWindow(object sender, RoutedEventArgs e)
        {
            NewLogin newLogin = new NewLogin();
            newLogin.Show();
        }

        private void deleteLogin(object sender, RoutedEventArgs e)
        {
            int index = LoginDataGrid.SelectedIndex;
            ConfirmationDialog confirmDialog = new ConfirmationDialog("Delete", index, LoginDataGrid);
            confirmDialog.Show();
        }

        private void getPassword(object sender, RoutedEventArgs e)
        {
            int index = LoginDataGrid.SelectedIndex;
            LoginEntry entry = (LoginEntry)LoginArrayHandler.INSTANCE.logins[index];
            String password = entry.generatePassword(entry.getUsername(), entry.getWebsite(), entry.getUnixTime(), App.masterPassword);
            Clipboard.SetText(password);
            password = "";
            LoginDataGrid.SelectedIndex = -1;
        }

        private void resetPassword(object sender, RoutedEventArgs e)
        {
            int index = LoginDataGrid.SelectedIndex;
            ConfirmationDialog confirmDialog = new ConfirmationDialog("Reset", index, LoginDataGrid);
            confirmDialog.Show();
        }
    }
}
