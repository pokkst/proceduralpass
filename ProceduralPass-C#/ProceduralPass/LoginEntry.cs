﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace ProceduralPass
{
    public class LoginEntry
    {
        public String Username { get; set; }
        public String Website { get; set; }
        public String unixTime = "";
        public String generatedPassword = "";
        public String[] saltTable = new String[] { "RF0VcwcU72Di/CViIyQ==", "tSzfiE9ILJ3GoeB6LrzDyi8=", "ScwxrfsCTsHD/BEI00Vx3A==", "+pXmuSPgJnm7+jXrA/r0w==", "+tNW3r9ewsgLJy4h#YY#Gzj==*#*YYT*" };

        public LoginEntry(String user, String site, String unixTime)
        {
            this.Username = user;
            this.Website = site;
            this.unixTime = unixTime;
        }

        public String getUsername()
        {
            return Username;
        }

        public String getWebsite()
        {
            return Website;
        }

        public String getUnixTime()
        {
            return unixTime;
        }

        public String generatePassword(String username, String site, String unixTime, String masterPassword)
        {
            String password = "";

            //This is v3 of the generation algorithm. I plan to phase out v1 and v2 within future updates. People should update their passwords.
            //This may be the final algorithm change for a while. I think it's good enough to obfuscate the master password.

            //Alright, here's how the bad boy works.
            /**
             * Here we take the username and site data and hash it with a hardcoded salt.
             **/
            int masterPassLength = masterPassword.Length;

            String saltedUsername = SHA256(username) + "*#UYT4348ghy*H&T^F&%RT^H*NG&^B(T(^*hg3u4g**H";
            String saltedWebsite = SHA256(site) + "*#UYTG*H3u4hg3u4g**H";
            String saltedMasterpass = masterPassLength.ToString() + SHA256(masterPassword) + "*^GBR%y7g6g6hj&T&^^%^F56d56D%F65f567f5&F57f%&f**H";

            /**
             * This is what I like to call the 3D hashing machine. We run it through these for loops, similar to a 3D objects x, y, and z axis.
             **/
            for (int x = 0; x < 32; x++)
            {
                for (int y = 0; y < 32; y++)
                {
                    for (int z = 0; z < 32; z++)
                    {
                        char[] charArray = App.availChars2.ToCharArray();

                        /**
                         * Next we run it through the 3D hashing machine to get a new username and website hash with random salt hashes generated on the fly,
                         * mixed with predefined salts in the String[] "saltTable".
                         **/
                        String nextUsernameSalt = "";
                        String nextWebsiteSalt = "";
                        String nextMasterpassSalt = "";
                        int randomSaltTableHashIndexUser = 0;
                        int randomSaltTableHashIndexSite = 0;
                        int randomSaltTableHashIndexMasterpass = 0;

                        for (int i = 0; i < 32; i++)
                        {
                            int threeDimSeedUser = (saltedWebsite.Length + saltedUsername.Length) + (x * x + y * y + (z * (z * 2))) + (i * 4);

                            RandomNumberGenerator threeDimRandomUser = new RandomNumberGenerator(threeDimSeedUser * 5, i + 5);
                            int userCharChosen = threeDimRandomUser.nextInt(App.availChars2.Length);
                            nextUsernameSalt += charArray[userCharChosen].ToString();
                            randomSaltTableHashIndexUser = threeDimRandomUser.nextInt(saltTable.Length);

                            int threeDimSeedSite = (256 + (saltedWebsite.Length + saltedUsername.Length) * (x + x + (y * y) + z + z) * i) * 2;

                            RandomNumberGenerator threeDimRandomSite = new RandomNumberGenerator(threeDimSeedSite, i + 2);
                            int siteCharChosen = threeDimRandomSite.nextInt(App.availChars2.Length);
                            nextWebsiteSalt += charArray[siteCharChosen].ToString();
                            randomSaltTableHashIndexSite = threeDimRandomSite.nextInt(saltTable.Length);

                            int threeDimSeedMasterpass = Math.Abs((saltedMasterpass.Length * 4) - (x + (x + y) * y + z * z) + (i * i));

                            RandomNumberGenerator threeDimRandomMasterpass = new RandomNumberGenerator(threeDimSeedMasterpass + 1, i + 1);
                            int mpCharChosen = threeDimRandomMasterpass.nextInt(App.availChars2.Length);
                            nextMasterpassSalt += charArray[mpCharChosen].ToString();
                            randomSaltTableHashIndexMasterpass = threeDimRandomMasterpass.nextInt(saltTable.Length);
                        }

                        saltedUsername = SHA256(saltedUsername + nextUsernameSalt) + saltTable[randomSaltTableHashIndexUser];
                        saltedWebsite = SHA256(saltedWebsite + nextWebsiteSalt) + saltTable[randomSaltTableHashIndexSite];
                        saltedMasterpass = SHA256(saltedMasterpass + nextMasterpassSalt) + saltTable[randomSaltTableHashIndexMasterpass];
                    }

                    /**
                     * The salted hashes are then hashed again in each loop on a different axis.
                     **/
                    saltedMasterpass = SHA256(saltedMasterpass);
                    saltedWebsite = SHA256(saltedWebsite);
                    saltedUsername = SHA256(saltedUsername);
                }

                /**
                 * The salted hashes are then hashed *again* on a another axis.
                 **/
                saltedMasterpass = SHA256(saltedMasterpass);
                saltedWebsite = SHA256(saltedWebsite);
                saltedUsername = SHA256(saltedUsername);
            }

            /**
             * To keep most hashes unique, as each hash's length is 64 characters long, meaning we'd get almost the same seed every time, we add up the UTF8 values of each character in the
             * string for a unique number. We do this for the masterpassword hash, and username hash.
             **/
            int mpHashCharSum = 0;
            int userHashCharSum = 0;
            int siteHashCharSum = 0;
            int mpHashCharProduct = 1;

            foreach (byte b in System.Text.Encoding.UTF8.GetBytes(saltedMasterpass.ToCharArray()))
            {
                mpHashCharSum += b;
                mpHashCharProduct *= b + mpHashCharSum;
            }

            foreach (byte b in System.Text.Encoding.UTF8.GetBytes(saltedUsername.ToCharArray()))
                userHashCharSum += b;

            foreach (byte b in System.Text.Encoding.UTF8.GetBytes(saltedWebsite.ToCharArray()))
                siteHashCharSum += b;

            /**
             * We then multiple the above UTF8 sums and add it to the length of the masterpassword hash, saltedWebsite hash, and the saltedUsername hash * 4, all of which is multiplied by 128.
             * We then set this as the next seed.
             * We then generate the length of the password using the masterpass UTF8 sum while dividing by the saltedUsername length, saltedWebsite length, and masterpass length, all 
             * multiplied by 4. Which all of *that* is multiplied by 6. Then we add 32 to ALL of *that* as a base length.
             **/
            int seed = ((mpHashCharSum * userHashCharSum) + masterPassLength + siteHashCharSum + (mpHashCharProduct * 4)) * 128;
            RandomNumberGenerator rand = new RandomNumberGenerator(seed, mpHashCharSum);
            int length = rand.nextInt((((mpHashCharSum + ((mpHashCharProduct + siteHashCharSum + masterPassLength) * 4)) * 6)) / 2000) + 32;

            for (int x = 0; x < length; x++)
            {
                /**
                 * We then run the length of the password in a for loop to use more procedural generation to choose characters from the list of available ones in App.availChars2.
                 **/
                RandomNumberGenerator charRand = new RandomNumberGenerator(rand.nextInt(masterPassLength * 32) * (x * x) + Int32.Parse(unixTime), mpHashCharSum);
                int charChosen = charRand.nextInt(App.availChars2.Length);
                char[] charArray = App.availChars2.ToCharArray();

                //The chosen character is then added onto the password string.
                password += charArray[charChosen].ToString();
            }

            //Then the data is cleared.
            masterPassword = "0000000000000000";
            site = "0000000000000000";
            username = "0000000000000000";
            masterPassLength = -1;
            //And the password is returned to us.
            return password;
        }

        private string SHA256(string input)
        {
            var crypt = new System.Security.Cryptography.SHA256Managed();
            var hash = new System.Text.StringBuilder();
            byte[] crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(input));
            foreach (byte theByte in crypto)
            {
                hash.Append(theByte.ToString("x2"));
            }
            return hash.ToString();
        }
    }
}
