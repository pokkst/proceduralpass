﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProceduralPass
{
    public class RandomNumberGenerator
    {
        private int max;
        private int last;

        public RandomNumberGenerator(int seed, int weight)
        {
            last = (int)(seed % weight);
        }
        // Note that the result can not be bigger then 999,999,999
        public int nextInt()
        {
            this.max = 999999999;
            last = (last * 32719 + 3) % 999999999;
            int number = last % max;
            if (number < 0)
            {
                number = number * -1;
            }
            return number;
        }

        public int nextInt(int max)
        {
            this.max = max;
            last = (last * 32719 + 3) % 999999999;
            int number = last % max;
            if (number < 0)
            {
                number = number * -1;
            }
            return number;
        }
    }
}
