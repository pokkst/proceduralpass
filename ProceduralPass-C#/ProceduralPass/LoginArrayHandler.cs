﻿using System;
using System.IO;
using System.Collections;

namespace ProceduralPass
{
    public class LoginArrayHandler
    {
        public static LoginArrayHandler INSTANCE;
        public ArrayList logins;

        public LoginArrayHandler()
        {
            INSTANCE = this;
        }

        public void saveLogins()
        {
            String data = "";
            foreach (LoginEntry login in logins)
            {
                data += login.getWebsite() + "=" + login.getUsername() + "=" + login.getUnixTime() +  Environment.NewLine;
            }

            File.WriteAllText("./data.txt", data);
        }

        public void loadLogins()
        {
            if (File.Exists("./data.txt"))
            {
                logins = new ArrayList();
                foreach (String line in File.ReadLines("./data.txt"))
                {
                    String[] combo = line.Split('=');
                    String website = combo[0];
                    String username = combo[1];
                    String unixTime = "";

                    if(combo.Length == 3)
                    {
                        unixTime = combo[2];
                    }

                    if (combo.Length == 4)
                    {
                        unixTime = combo[2];
                    }

                    logins.Add(new LoginEntry(username, website, unixTime));
                }
            }
            else
            {
                logins = new ArrayList();
            }
        }
    }
}
