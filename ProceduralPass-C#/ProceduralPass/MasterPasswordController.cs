﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Controls;
using System.Windows;
using System.Security.Cryptography;

namespace ProceduralPass
{
    public class MasterPasswordController : ICommand
    {
        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            PasswordBox passBox = (PasswordBox)parameter;

            if (passBox.Password != "")
            {
                App.masterPassword = passBox.Password;
                App.masterPassword = SHA256(App.masterPassword + "&GH&*BT6VR6r6v6&B&H7H7h&H77ybn&y7TBV");
                passBox.Password = "000000000000";
                var w = Application.Current.Windows[0];
                Logins loginWindow = new Logins();
                loginWindow.Show();
                w.Close();
            }
        }

        private string SHA256(string input)
        {
            var crypt = new SHA256Managed();
            var hash = new System.Text.StringBuilder();
            byte[] crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(input));
            foreach (byte theByte in crypto)
            {
                hash.Append(theByte.ToString("x2"));
            }
            return hash.ToString();
        }
    }
}
