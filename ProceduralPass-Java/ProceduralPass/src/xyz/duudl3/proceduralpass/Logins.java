package xyz.duudl3.proceduralpass;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.UnsupportedEncodingException;

import javax.swing.BorderFactory;
import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;

public class Logins extends Canvas
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6359243402452808939L;
	private JFrame frame;
	public static Logins INSTANCE;
	public Object[][] rowData;
	public static Object columnNames[] = { "Password", "Reset Password", "Delete", "Username", "Website" };
	public JTable table;
	public JScrollPane scrollPane;
	public JPanel mainPanel;

	public Logins()
	{
		INSTANCE = this;
		mainPanel = new JPanel();
		this.setPreferredSize(new Dimension(800, 450));
		this.setMaximumSize(new Dimension(800, 450));
		this.setMinimumSize(new Dimension(800, 450));

		/***LABEL FIELD START***/
		JLabel label = new JLabel("Logins");
		if(MainWindow.INSTANCE.getOS().toLowerCase().contains("linux"))
		{
			label.setFont(new Font("Arial", Font.PLAIN, 24));
			label.setBounds(10, 5, 150, 35);
		}
		else
		{
			label.setFont(new Font("Arial", Font.PLAIN, 24));
			label.setBounds(10, 5, 75, 35);
		}

		label.setForeground(Color.white);
		label.setBorder(null);
		label.setOpaque(false);
		label.setHorizontalTextPosition(JButton.CENTER);
		label.setBackground(new Color(0, 0, 0, 0));
		label.setVerticalTextPosition(JButton.CENTER);
		/***LABEL FIELD END***/

		/***ADD LOGIN BUTTON START**/
		JButton add = new JButton("Add Login");
		add.setFont(new Font("Arial", Font.PLAIN, 14));
		add.setBorder(null);
		add.setFocusPainted(false);
		add.setBorderPainted(false);
		add.setForeground(Color.white);
		add.setBackground(Color.decode("#666666"));
		add.setContentAreaFilled(true);
		add.setBounds((int)(this.getMaximumSize().getWidth()) - 140, 12, 120, 30);
		add.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				if(App.masterPassword.length() != 0)
				{
					NewLogin newLogin = new NewLogin();
					newLogin.show();
				}
			}
		});
		/***ADD LOGIN BUTTON END***/

		/***LOGIN TABLE START***/
		if(LoginArrayHandler.INSTANCE.logins.size() != 0)
		{
			rowData = null;
			int size = LoginArrayHandler.INSTANCE.logins.size();
			rowData = new Object[size][5];

			for(int x = 0; x < size; x++)
			{
				for(int y = 0; y < 5; y++)
				{
					if(y == 3)
						rowData[x][y] = LoginArrayHandler.INSTANCE.logins.get(x).getUsername();
					if(y == 4)
						rowData[x][y] = LoginArrayHandler.INSTANCE.logins.get(x).getWebsite();
				}
			}

			table = new JTable(rowData, columnNames);

			scrollPane = new JScrollPane(table);
			setTableRendering();
		}
		else
		{
			rowData = new Object[0][5];
			table = new JTable(rowData, columnNames);

			scrollPane = new JScrollPane(table);
			setTableRendering();
		}
		/***LOGIN TABLE END***/

		mainPanel.setBackground(Color.decode("#1D1D1D"));
		mainPanel.setOpaque(true);
		this.setBackground(Color.decode("#1D1D1D"));
		mainPanel.add(this);
		mainPanel.add(label);
		mainPanel.add(add);
		mainPanel.add(scrollPane);
		mainPanel.setLayout (null); 

		frame = new JFrame(MainWindow.title + " " + MainWindow.version);
		frame.setSize(800, 450);
		frame.getContentPane().add(mainPanel);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		frame.setUndecorated(false);
	}

	public void show()
	{
		frame.setVisible(true);
	}

	public void hide()
	{
		frame.setVisible(false);
		frame.dispose();
	}

	public void close()
	{
		frame.dispose();
		frame = null;
	}

	public void deleteLogin()
	{
		int index = Logins.INSTANCE.table.getSelectedRow();
		LoginArrayHandler.INSTANCE.logins.remove(index);
		LoginArrayHandler.INSTANCE.saveLogins();

		Logins.INSTANCE.mainPanel.remove(Logins.INSTANCE.scrollPane);
		Logins.INSTANCE.scrollPane = null;

		Logins.INSTANCE.rowData = null;
		int size = LoginArrayHandler.INSTANCE.logins.size();
		Logins.INSTANCE.rowData = new Object[size][5];

		for(int x = 0; x < size; x++)
		{
			for(int y = 0; y < 5; y++)
			{
				if(y == 3)
					Logins.INSTANCE.rowData[x][y] = LoginArrayHandler.INSTANCE.logins.get(x).getUsername();
				if(y == 4)
					Logins.INSTANCE.rowData[x][y] = LoginArrayHandler.INSTANCE.logins.get(x).getWebsite();
			}
		}

		Logins.INSTANCE.table = new JTable(Logins.INSTANCE.rowData, Logins.columnNames);
		Logins.INSTANCE.scrollPane = new JScrollPane(Logins.INSTANCE.table);

		setTableRendering();

		Logins.INSTANCE.mainPanel.add(Logins.INSTANCE.scrollPane);
	}

	public void getPassword()
	{
		int index = Logins.INSTANCE.table.getSelectedRow();
		LoginEntry entry = (LoginEntry)LoginArrayHandler.INSTANCE.logins.get(index);
		String password = null;
		try {
			password = entry.generatePassword(entry.getUsername(), entry.getWebsite(), entry.getUnixTime(), App.masterPassword);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		StringSelection stringSelection = new StringSelection(password);
		Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
		clpbrd.setContents(stringSelection, null);
		password = "";
		Logins.INSTANCE.table.clearSelection();
	}

	public void resetPassword()
	{
		int index = Logins.INSTANCE.table.getSelectedRow();
		LoginEntry entry = (LoginEntry)LoginArrayHandler.INSTANCE.logins.get(index);
		long unixTimestamp = System.currentTimeMillis() / 1000L;
		LoginArrayHandler.INSTANCE.logins.set(index, new LoginEntry(entry.getUsername(), entry.getWebsite(), unixTimestamp + ""));
		entry = (LoginEntry)LoginArrayHandler.INSTANCE.logins.get(index);
		String password = null;
		try {
			password = entry.generatePassword(entry.getUsername(), entry.getWebsite(), entry.getUnixTime(), App.masterPassword);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		LoginArrayHandler.INSTANCE.saveLogins();
		StringSelection stringSelection = new StringSelection(password);
		Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
		clpbrd.setContents(stringSelection, null);
		password = "";
		Logins.INSTANCE.table.clearSelection();
	}

	public void setTableRendering()
	{
		scrollPane.setBackground(Color.decode("#303030"));
		table.setBackground(Color.decode("#303030"));
		table.setShowGrid(false);
		table.setBorder(null);
		table.setSelectionBackground(Color.lightGray);
		table.setForeground(Color.decode("#FFFFFF"));
		table.setDragEnabled(false);
		table.getTableHeader().setReorderingAllowed(false);
		table.getTableHeader().setDefaultRenderer(new SimpleHeaderRenderer());
		JTableHeader header = table.getTableHeader();
		header.setBackground(Color.decode("#666666"));
		header.setForeground(Color.white);
		header.setBorder(null);
		table.getColumn("Password").setCellRenderer(new ButtonRenderer("Get"));
		table.getColumn("Password").setCellEditor(new GetPasswordButtonEditor(new JCheckBox()));
		table.getColumn("Reset Password").setCellRenderer(new ButtonRenderer("Reset"));
		table.getColumn("Reset Password").setCellEditor(new ResetButtonEditor(new JCheckBox()));
		table.getColumn("Delete").setCellRenderer(new ButtonRenderer("Delete"));
		table.getColumn("Delete").setCellEditor(new DeleteButtonEditor(new JCheckBox()));
		Dimension dim = new Dimension(2,2);
		table.setIntercellSpacing(new Dimension(dim));
		scrollPane.setBounds(10, 50, (int)this.getMaximumSize().getWidth() - 25, 360);
		scrollPane.getViewport().setBackground(Color.decode("#303030"));
		scrollPane.getViewport().setBorder(null);
		scrollPane.setViewportBorder(null);
		scrollPane.setBorder(BorderFactory.createEmptyBorder());
	}

	public class ButtonRenderer extends JButton implements TableCellRenderer {

		/**
		 * 
		 */
		private static final long serialVersionUID = -9080091561853684230L;
		private String text;
		public ButtonRenderer(String text)
		{
			this.text = text;
			setOpaque(true);
			setBorder(null);
			setBorderPainted(false);
		}

		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
			if (isSelected) {
				setForeground(table.getSelectionForeground());
				setBackground(table.getSelectionBackground());
			} else {
				setForeground(table.getForeground());
				setBackground(Color.decode("#666666"));
			}
			setText(text);
			return this;
		}
	}

	/**
	 * @version 1.0 11/09/98
	 */

	public class GetPasswordButtonEditor extends DefaultCellEditor {
		/**
		 * 
		 */
		private static final long serialVersionUID = -4889305157949569345L;

		protected JButton button;

		private String label;

		private boolean isPushed;

		public GetPasswordButtonEditor(JCheckBox checkBox) {
			super(checkBox);
			button = new JButton();
			button.setOpaque(true);
			button.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					fireEditingStopped();
				}
			});
		}

		public Component getTableCellEditorComponent(JTable table, Object value,
				boolean isSelected, int row, int column) {
			if (isSelected) {
				button.setForeground(table.getSelectionForeground());
				button.setBackground(table.getSelectionBackground());
			} else {
				button.setForeground(table.getForeground());
				button.setBackground(table.getBackground());
			}
			label = (value == null) ? "" : value.toString();
			button.setText(label);
			isPushed = true;
			return button;
		}

		public Object getCellEditorValue() {
			if (isPushed) {
				Logins.INSTANCE.getPassword();
			}
			isPushed = false;
			return new String(label);
		}

		public boolean stopCellEditing() {
			isPushed = false;
			return super.stopCellEditing();
		}

		protected void fireEditingStopped() {
			super.fireEditingStopped();
		}
	}

	public class ResetButtonEditor extends DefaultCellEditor {

		/**
		 * 
		 */
		private static final long serialVersionUID = 3589333262155472510L;

		protected JButton button;

		private String label;

		private boolean isPushed;

		public ResetButtonEditor(JCheckBox checkBox) {
			super(checkBox);
			button = new JButton();
			button.setOpaque(true);
			button.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					fireEditingStopped();
				}
			});
		}

		public Component getTableCellEditorComponent(JTable table, Object value,
				boolean isSelected, int row, int column) {
			if (isSelected) {
				button.setForeground(table.getSelectionForeground());
				button.setBackground(table.getSelectionBackground());
			} else {
				button.setForeground(table.getForeground());
				button.setBackground(table.getBackground());
			}
			label = (value == null) ? "" : value.toString();
			button.setText(label);
			isPushed = true;
			return button;
		}

		public Object getCellEditorValue() {
			if (isPushed) {
				// 
				// 
				//JOptionPane.showMessageDialog(button, label + ": Ouch!");
				// System.out.println(label + ": Ouch!");
				ConfirmationDialog confirmDialog = new ConfirmationDialog("Reset");
				confirmDialog.show();
			}
			isPushed = false;
			return new String(label);
		}

		public boolean stopCellEditing() {
			isPushed = false;
			return super.stopCellEditing();
		}

		protected void fireEditingStopped() {
			super.fireEditingStopped();
		}
	}

	public class DeleteButtonEditor extends DefaultCellEditor 
	{
		/**
		 * 
		 */
		private static final long serialVersionUID = -509316890982779600L;

		protected JButton button;

		private String label;

		private boolean isPushed;

		public DeleteButtonEditor(JCheckBox checkBox) {
			super(checkBox);
			button = new JButton();
			button.setOpaque(true);
			button.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					fireEditingStopped();
				}
			});
		}

		public Component getTableCellEditorComponent(JTable table, Object value,
				boolean isSelected, int row, int column) {
			if (isSelected) {
				button.setForeground(table.getSelectionForeground());
				button.setBackground(table.getSelectionBackground());
			} else {
				button.setForeground(table.getForeground());
				button.setBackground(table.getBackground());
			}
			label = (value == null) ? "" : value.toString();
			button.setText(label);
			isPushed = true;
			return button;
		}

		public Object getCellEditorValue() {
			if (isPushed) {
				// 
				// 
				//JOptionPane.showMessageDialog(button, label + ": Ouch!");
				// System.out.println(label + ": Ouch!");
				ConfirmationDialog confirmDialog = new ConfirmationDialog("Delete");
				confirmDialog.show();
			}
			isPushed = false;
			return new String(label);
		}

		public boolean stopCellEditing() {
			isPushed = false;
			return super.stopCellEditing();
		}

		protected void fireEditingStopped() {
			super.fireEditingStopped();
		}
	}

	public class SimpleHeaderRenderer extends JLabel implements TableCellRenderer {

		/**
		 * 
		 */
		private static final long serialVersionUID = 8875291031751751658L;

		public SimpleHeaderRenderer() {
			setFont(new Font("Arial", Font.PLAIN, 14));
			setHorizontalAlignment(JButton.CENTER);
			setForeground(Color.decode("#FFFFFF"));
			setBorder(BorderFactory.createEmptyBorder());
		}

		@Override
		public Component getTableCellRendererComponent(JTable table, Object value,
				boolean isSelected, boolean hasFocus, int row, int column) {
			setText(value.toString());
			return this;
		}

	}
}