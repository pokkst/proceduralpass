# Procedural Pass
Procedural Pass is a password generator/manager that uses procedural generation to generate your passwords on the fly. Simply enter a master password of your choice (and be sure to remember it), add a login entry, and use the password that is procedurally generated for you. When launching the program again, be sure to enter the same master password to get your passwords back.

(Transferred over my old GitLab account)

Build Instructions
---
C#
---
* Download Visual Studio Community: https://www.visualstudio.com/downloads/
* Open the project using the solution (.sln) file located in [ProceduralPass-C#](https://github.com/duudl3/proceduralpass/tree/master/ProceduralPass-C%23)
* After opening Visual Studio Community, click the "Build" tab, press "Build Solution". 

The .exe will be placed in the ProceduralPass/bin/Release folder, or ProceduralPass/bin/Debug folder.

Java
---
I'm assuming you already have the Java JDK installed and set up.
* Download Eclipse: https://eclipse.org
* Open the folder "ProceduralPass-Java" in Eclipse.
* Click the "File" button at the top, then click "Export".
* Expand the Java folder in the window that appears and choose "Runnable JAR File".
* For the launch configuration, choose "MainWindow - ProceduralPass".
* Choose your export destination.
* For Library handling, choose "Package required libraries into generated JAR".
* Click "Finish".

The .jar will be generated and can be used on Windows, Linux, or macOS (macOS still has yet to be tested though).
