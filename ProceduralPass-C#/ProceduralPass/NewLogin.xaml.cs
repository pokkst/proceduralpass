﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Text.RegularExpressions;

namespace ProceduralPass
{
    /// <summary>
    /// Interaction logic for NewLogin.xaml
    /// </summary>
    public partial class NewLogin : Window
    {
        public NewLogin()
        {
            InitializeComponent();
        }

        private void addNewLogin(object sender, RoutedEventArgs e)
        {
            if (Username.Text != "" && Website.Text != "" && !Username.Text.Contains("=") && !Website.Text.Contains("="))
            {
                Logins.INSTANCE.LoginDataGrid.ItemsSource = null;
                Int32 unixTimestamp = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                LoginArrayHandler.INSTANCE.logins.Add(new LoginEntry(Username.Text, Website.Text, unixTimestamp.ToString()));
                Logins.INSTANCE.LoginDataGrid.ItemsSource = LoginArrayHandler.INSTANCE.logins;
                LoginArrayHandler.INSTANCE.saveLogins();
                Close();
            }
        }
    }
}
