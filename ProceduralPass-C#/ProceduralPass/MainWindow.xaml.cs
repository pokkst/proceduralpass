﻿using System.Windows;
using System.IO;
using System;
using System.Collections;

namespace ProceduralPass
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MasterPasswordController passCmd { get; set; }

        public MainWindow()
        {
            LoginArrayHandler loginHandler = new LoginArrayHandler();

            InitializeComponent();
            DataContext = this;
            passCmd = new MasterPasswordController();

            loginHandler.loadLogins();
            loginHandler.saveLogins();
        }
    }
}
