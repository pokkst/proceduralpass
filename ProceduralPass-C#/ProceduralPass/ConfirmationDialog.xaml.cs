﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProceduralPass
{
    /// <summary>
    /// Interaction logic for ConfirmationDialog.xaml
    /// </summary>
    public partial class ConfirmationDialog : Window
    {
        private String actionType = "";
        private int selectedRow = -1;
        private DataGrid dataGrid = null;

        public ConfirmationDialog(String actionType, int selectedRow, DataGrid dataGrid)
        {
            InitializeComponent();
            if(actionType == "Delete")
            {
                ConfirmDialogText.Content = "Are you sure you wish to delete?";
            }
            else if(actionType == "Reset")
            {
                ConfirmDialogText.Content = "Are you sure you wish to reset?";
            }

            this.actionType = actionType;
            this.selectedRow = selectedRow;
            this.dataGrid = dataGrid;
        }

        private void ConfirmAction(object sender, RoutedEventArgs e)
        {
            if (actionType == "Delete")
            {
                dataGrid.ItemsSource = null;
                LoginArrayHandler.INSTANCE.logins.RemoveAt(selectedRow);
                dataGrid.ItemsSource = LoginArrayHandler.INSTANCE.logins;
                LoginArrayHandler.INSTANCE.saveLogins();
            }
            else if(actionType == "Reset")
            {
                LoginEntry entry = (LoginEntry)LoginArrayHandler.INSTANCE.logins[selectedRow];
                Int32 unixTimestamp = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                LoginArrayHandler.INSTANCE.logins[selectedRow] = new LoginEntry(entry.getUsername(), entry.getWebsite(), unixTimestamp.ToString());
                entry = (LoginEntry)LoginArrayHandler.INSTANCE.logins[selectedRow];
                String password = entry.generatePassword(entry.getUsername(), entry.getWebsite(), entry.getUnixTime(), App.masterPassword);
                LoginArrayHandler.INSTANCE.saveLogins();
                Clipboard.SetText(password);
                password = "";
                dataGrid.SelectedIndex = -1;
            }

            Close();
        }

        private void DeclineAction(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
