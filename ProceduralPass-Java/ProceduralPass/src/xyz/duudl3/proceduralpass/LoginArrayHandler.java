package xyz.duudl3.proceduralpass;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class LoginArrayHandler {
	public static LoginArrayHandler INSTANCE;
	public ArrayList<LoginEntry> logins;
	private static final String saveFileName = "data.txt";
	
	public LoginArrayHandler()
	{
		INSTANCE = this;
	}

	public void saveLogins()
	{
		File dir = new File("./" + saveFileName);
		
		BufferedWriter writer = null;
		try 
		{
			writer = new BufferedWriter(new FileWriter(dir));

			try 
			{
				for(LoginEntry login : logins)
				{
					writer.write(login.getWebsite() + "=" + login.getUsername() + "=" + login.getUnixTime());
					writer.newLine();
				}
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}

		try {
			writer.flush();
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		writer = null;
	}

	public void loadLogins()
	{
		File f = new File("./data.txt");
		if(f.exists() && !f.isDirectory()) { 
			logins = new ArrayList<LoginEntry>();
			try (BufferedReader br = new BufferedReader(new FileReader("./data.txt"))) {
				String line;
				while ((line = br.readLine()) != null) {
					String[] combo = line.split("=");
					String website = combo[0];
					String username = combo[1];
					String unixTime = "";

					if(combo.length == 3)
					{
						unixTime = combo[2];
					}

					//after v1.3.0 this will be phased out slowly.
					if (combo.length == 4)
					{
						unixTime = combo[2];
					}

					logins.add(new LoginEntry(username, website, unixTime));
				}
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else
		{
			logins = new ArrayList<LoginEntry>();
		}
	}
}
