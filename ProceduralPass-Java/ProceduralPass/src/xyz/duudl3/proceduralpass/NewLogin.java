package xyz.duudl3.proceduralpass;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

public class NewLogin extends Canvas implements KeyListener
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -4657998640626112299L;
	private JFrame frame;
	private JTextField username;
	private JTextField website;
	public NewLogin()
	{
		JPanel mainPanel = new JPanel();
		this.setPreferredSize(new Dimension(400, 200));
		this.setMaximumSize(new Dimension(400, 200));
		this.setMinimumSize(new Dimension(400, 200));

		/***LABEL FIELD START***/
		JLabel userLabel = new JLabel("Username");
		userLabel.setFont(new Font("Arial", Font.PLAIN, 24));
		userLabel.setForeground(Color.white);
		userLabel.setBorder(null);
		userLabel.setOpaque(false);
		userLabel.setHorizontalTextPosition(JButton.CENTER);
		userLabel.setBackground(new Color(0, 0, 0, 0));
		userLabel.setVerticalTextPosition(JButton.CENTER);
		userLabel.setBounds(10, 15, 185, 25);
		/***LABEL FIELD END***/

		/***USERNAME FIELD START***/
		username = new JTextField("");
		username.setFont(new Font("Arial", Font.PLAIN, 14));
		username.setBorder(null);
		username.setBounds(10, 45, 200, 25);
		username.setBackground(Color.decode("#3C3C3C"));
		username.setForeground(Color.white);
		username.setCaretColor(Color.white);
		/***USERNAME FIELD END***/

		/***LABEL FIELD START***/
		JLabel siteLabel = new JLabel("Website");
		siteLabel.setFont(new Font("Arial", Font.PLAIN, 24));
		siteLabel.setForeground(Color.white);
		siteLabel.setBorder(null);
		siteLabel.setOpaque(false);
		siteLabel.setHorizontalTextPosition(JButton.CENTER);
		siteLabel.setBackground(new Color(0, 0, 0, 0));
		siteLabel.setVerticalTextPosition(JButton.CENTER);
		siteLabel.setBounds(10, 85, 185, 25);
		/***LABEL FIELD END***/

		/***USERNAME FIELD START***/
		website = new JTextField("");
		website.setFont(new Font("Arial", Font.PLAIN, 14));
		website.setBorder(null);
		website.setBounds(10, 115, 200, 25);
		website.setBackground(Color.decode("#3C3C3C"));
		website.setForeground(Color.white);
		website.setCaretColor(Color.white);
		website.addKeyListener(this);
		/***USERNAME FIELD END***/

		/***LOGIN BUTTON START**/
		JButton add = new JButton("Add");
		add.setFont(new Font("Arial", Font.PLAIN, 14));
		add.setBorder(null);
		add.setBorderPainted(false);
		add.setForeground(Color.white);
		add.setBackground(Color.decode("#666666"));
		add.setContentAreaFilled(true);
		add.setBounds((int)(this.getMaximumSize().getWidth()) - 140, (int)this.getMaximumSize().getHeight() - 85, 120, 30);
		add.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				if(username.getText().length() != 0 && website.getText().length() != 0 && !username.getText().contains("=") && !website.getText().contains("="))
				{
					addNewLogin(username.getText(), website.getText());

					Logins.INSTANCE.mainPanel.remove(Logins.INSTANCE.scrollPane);
					Logins.INSTANCE.scrollPane = null;

					Logins.INSTANCE.rowData = null;
					int size = LoginArrayHandler.INSTANCE.logins.size();
					Logins.INSTANCE.rowData = new Object[size][5];

					for(int x = 0; x < size; x++)
					{
						for(int y = 0; y < 5; y++)
						{
							if(y == 3)
								Logins.INSTANCE.rowData[x][y] = LoginArrayHandler.INSTANCE.logins.get(x).getUsername();
							if(y == 4)
								Logins.INSTANCE.rowData[x][y] = LoginArrayHandler.INSTANCE.logins.get(x).getWebsite();
						}
					}

					Logins.INSTANCE.table = new JTable(Logins.INSTANCE.rowData, Logins.columnNames);
					Logins.INSTANCE.scrollPane = new JScrollPane(Logins.INSTANCE.table);
					
					Logins.INSTANCE.setTableRendering();
					
					Logins.INSTANCE.mainPanel.add(Logins.INSTANCE.scrollPane);
				}
			}
		});
		/***LOGIN BUTTON END***/

		mainPanel.setBackground(Color.decode("#1D1D1D"));

		mainPanel.setOpaque(true);
		this.setBackground(Color.decode("#1D1D1D"));
		mainPanel.add(this);
		mainPanel.add(userLabel);
		mainPanel.add(add);
		mainPanel.add(username);
		mainPanel.add(siteLabel);
		mainPanel.add(website);
		mainPanel.setLayout (null); 

		frame = new JFrame("Add New Login");
		frame.setSize(400, 200);
		frame.getContentPane().add(mainPanel);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		frame.setUndecorated(false);	
	}

	private void addNewLogin(String username, String website)
	{
		if (username != "" && website != "")
		{
			long unixTimestamp = System.currentTimeMillis() / 1000L;
			LoginArrayHandler.INSTANCE.logins.add(new LoginEntry(username, website, unixTimestamp + ""));
			LoginArrayHandler.INSTANCE.saveLogins();
			close();
		}
	}

	public void show()
	{
		frame.setVisible(true);
	}

	public void hide()
	{
		frame.setVisible(false);
		frame.dispose();
	}

	public void close()
	{
		frame.dispose();
		frame = null;
	}

	@Override
	public void keyPressed(KeyEvent e) {
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		if(e.getKeyCode() == 10)
		{
			if(username.getText().length() != 0 && website.getText().length() != 0)
			{
				addNewLogin(username.getText(), website.getText());

				Logins.INSTANCE.mainPanel.remove(Logins.INSTANCE.scrollPane);
				Logins.INSTANCE.scrollPane = null;

				Logins.INSTANCE.rowData = null;
				int size = LoginArrayHandler.INSTANCE.logins.size();
				Logins.INSTANCE.rowData = new Object[size][5];

				for(int x = 0; x < size; x++)
				{
					for(int y = 0; y < 5; y++)
					{
						if(y == 3)
							Logins.INSTANCE.rowData[x][y] = LoginArrayHandler.INSTANCE.logins.get(x).getUsername();
						if(y == 4)
							Logins.INSTANCE.rowData[x][y] = LoginArrayHandler.INSTANCE.logins.get(x).getWebsite();
					}
				}

				Logins.INSTANCE.table = new JTable(Logins.INSTANCE.rowData, Logins.columnNames);
				Logins.INSTANCE.scrollPane = new JScrollPane(Logins.INSTANCE.table);
				
				Logins.INSTANCE.setTableRendering();
				
				Logins.INSTANCE.mainPanel.add(Logins.INSTANCE.scrollPane);
			}
		}
	}

	@Override
	public void keyTyped(KeyEvent e) {
		
	}
}
